extends Area2D

@export var speed = 400 # How fast the player will move (pixels/sec).
@export var beer_scene: PackedScene

var screen_size # Size of the game window.

func _ready():
    screen_size = get_viewport_rect().size

func _physics_process(delta):
    if Input.is_action_pressed("throw_beer"):
        if $ThrowCooldown.is_stopped():
            $ThrowCooldown.start()
            var beer = beer_scene.instantiate()
            add_child(beer)

    for child in get_children():
        if not child.is_in_group("Beer"): continue
        if(child.position.x > screen_size.x or child.position.y > screen_size.y):
            child.queue_free()

func _process(delta):
    var velocity = Vector2.ZERO # The player's movement vector.
    if Input.is_action_pressed("move_right"):
        velocity.x += 1
    if Input.is_action_pressed("move_left"):
        velocity.x -= 1
    if Input.is_action_pressed("move_down"):
        velocity.y += 1
    if Input.is_action_pressed("move_up"):
        velocity.y -= 1

    if velocity.length() > 0:
        velocity = velocity.normalized() * speed

    position += velocity * delta
    position = position.clamp(Vector2.ZERO, screen_size)
