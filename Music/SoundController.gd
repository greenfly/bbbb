extends Node

var audio_stream_player: AudioStreamPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
    audio_stream_player = get_node("AudioStreamPlayer")
    Events.connect("start", _on_start)
    Events.connect("reset", _on_reset)


func _on_start():
    audio_stream_player.stream_paused = true

func _on_reset():
    audio_stream_player.stream_paused = false
