extends Node2D

var bear_count : int

func _ready():
    for bear in $AverageBears.get_children():
        var velocity = Vector2(randf_range(15.0, 25.0), 0.0)
        var direction = randf_range(-PI, PI)
        bear.linear_velocity = velocity.rotated(direction)

func _physics_process(delta):
    bear_count = $AverageBears.get_children().size()
    if(bear_count == 0):
        Events.emit_signal("end")
