extends Node2D

var head : Sprite2D
var hat : Sprite2D
var body : Sprite2D
var rigidbod : AverageBear
var is_ready = false

var current_body_index = 0
var timer = 0
var max_walk_time = 0.5

var downed_bear = preload("res://World/FullBears/SplatBear.tscn")


# Called when the node enters the scene tree for the first time.
func _ready():
    head = get_node("BearMass/Head")
    hat = get_node("BearMass/Hat")
    body = get_node("BearMass/Body")
    rigidbod = get_node("BearMass")

    var neck = randi_range(-14,16)
    head.position+=Vector2(0,neck)
    hat.position+=Vector2(0,neck)

    head.texture = TexturePreloading.heads[randi_range(0, TexturePreloading.heads.size()-1)]
    hat.texture = TexturePreloading.hats[randi_range(0, TexturePreloading.hats.size()-1)]

    current_body_index = 0
    timer = 0
    max_walk_time = 0.5
    is_ready = true
    Events.connect("bear_splat",_on_splat_bear)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    if(is_ready):
        timer+=delta
        if(timer > max_walk_time):
            if(randf() < 0.01*max_walk_time):
                _on_splat_bear()
            else:
                timer = 0
                current_body_index = increase_step(current_body_index, TexturePreloading.walking.size())
                if(is_instance_valid(body)):
                    body.texture = TexturePreloading.walking[current_body_index];
                else:
                    is_ready = false

func increase_step(index, max_size) -> int:
    return (index+1) % max_size


func _on_splat_bear():
    is_ready = false
    var pos = get_node("BearMass").transform.origin
    var splat_bear: Sprite2D = downed_bear.instantiate()
    splat_bear.transform.origin = pos
    rigidbod._bear_has_left()
    add_child(splat_bear)
