extends VBoxContainer

var scores = []

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    pass

func add_to_score(new_score):
    if(scores.is_empty()):
        scores.append(new_score)
    var replaced = false
    for i in range(0, scores.size()):
        if(new_score > scores[i]):
            scores[i] = new_score
            replaced = true
            break
    if(!replaced && scores.size() < 11):
        scores.append(new_score)
