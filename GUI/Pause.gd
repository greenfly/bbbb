extends Node

var menu_box : VBoxContainer
# Called when the node enters the scene tree for the first time.
func _ready():
    Events.connect("end",_end)
    menu_box = get_node("../../VBoxContainer")
    pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    if(Input.is_action_just_pressed("Pause")):
        if(menu_box.visible):
            menu_box.hide()
        else:
            menu_box.show()

func _end():
    menu_box.show()
