extends Button

var main_scene = preload("res://World/THE_BAR.tscn")
var menu_box : VBoxContainer
var hud: Control

# Called when the node enters the scene tree for the first time.
func _ready():
    menu_box = get_node("../../VBoxContainer")
    hud = get_node("../../Hud")
    self.pressed.connect(self._button_pressed)
    Events.connect("reset", _on_reset)
    Events.connect("start", _on_start)

func _button_pressed():
    Events.emit_signal("start")
    menu_box.hide()
    hud.show()
    #menu_box.set_anchors_preset(1)
    get_tree().root.add_child(main_scene.instantiate())

func _on_reset():
    self.show()

func _on_start():
    self.hide()
