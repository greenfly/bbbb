extends Label


# Called when the node enters the scene tree for the first time.
func _ready():
    Events.connect("start", _on_start)
    Events.connect("reset", _on_reset)

func _on_start():
    self.hide()

func _on_reset():
    self.show()
