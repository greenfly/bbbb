extends Control

var label: Label

# Called when the node enters the scene tree for the first time.
func _ready():
    Events.connect("end",_show_ending)
    Events.connect("reset",_hide_ending)
    label = get_node("Label")

func _show_ending():
    self.show()
    
func _hide_ending():
    self.hide()
