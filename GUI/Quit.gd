extends Button

var hud : Control

func _ready():
    hud = get_node("../../Hud")
    self.pressed.connect(self._button_pressed)
    Events.connect("start", _on_start)


func _button_pressed():
    Events.emit_signal("reset")
    hud.hide()
    var world = get_tree().root.get_node("TheBar")
    # Be nice and dont immediately crash if button is spammed
    if(world):
        get_tree().root.remove_child(world)
        world.queue_free()
    self.hide()

func _on_start():
    self.show()
