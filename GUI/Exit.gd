extends Button


# Called when the node enters the scene tree for the first time.
func _ready():
    self.pressed.connect(self._button_pressed)
    Events.connect("reset", _on_reset)
    Events.connect("start", _on_start)


func _button_pressed():
    get_tree().root.propagate_notification(NOTIFICATION_WM_CLOSE_REQUEST)

func _notification(what):
    if what == NOTIFICATION_WM_CLOSE_REQUEST:
        get_tree().quit() # default behavior

func _on_reset():
    self.show()

func _on_start():
    self.hide()

