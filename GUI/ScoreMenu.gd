extends HBoxContainer

var label : Label
var score : Label

var score_value : int = 0

# Called when the node enters the scene tree for the first time.
func _ready():
    label = get_node("Label")
    score = get_node("Score")
    score_value = 0
    Events.connect("bear_hit_something",_bear_hit_something)
    Events.connect("beer_hit_something",_beer_hit_something)
    Events.connect("score_add",_add_score)
    Events.connect("score_sub",_sub_score)
    Events.connect("reset",_on_reset)

func _process(delta):
    if(Input.is_action_just_pressed("increase_score")):
        Events.emit_signal("score_add",1)
    if(Input.is_action_just_pressed("decrease_score")):
        Events.emit_signal("score_sub",1)
    if(Input.is_action_just_pressed("end")):
        Events.emit_signal("end")


func _bear_hit_something(bear, something):
    Events.emit_signal("score_add",1)

func _beer_hit_something(beer, something):
    Events.emit_signal("score_add",1)


func _add_score(value: int):
    score_value+=value
    score.text = String.num_int64(score_value)

func _sub_score(value: int):
    score_value-=value
    score.text = String.num_int64(score_value)

func _on_reset():
    Events.emit_signal("new_score",score_value)
    score_value = 0
    score.text = String.num_int64(score_value)



