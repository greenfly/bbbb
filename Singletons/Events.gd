extends Node

signal score_add(value)
signal score_sub(value)
signal new_score(value)

signal end(score)

signal start()
signal pause()
signal reset()

signal bear_splat()

signal bear_hit_something(bear, something)
signal beer_hit_something(bottle, something)
signal beer_left(position)
signal bear_left(position)
