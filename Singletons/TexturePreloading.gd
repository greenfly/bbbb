extends Node

var heads = []
var hats = []
var walking = []
# Called when the node enters the scene tree for the first time.
func _ready():
    #heads.append(load("res://assets/Bears/Heads/cap_head.png"))
    #heads.append(load("res://assets/Bears/Heads/drooling_head.png"))
    #heads.append(load("res://assets/Bears/Heads/cap_ears_head.png"))
    #heads.append(load("res://assets/Bears/Heads/western_bear.png"))
    #heads.append(load("res://assets/Bears/Heads/blindfolded_bear.png"))
    #heads.append(load("res://assets/Bears/Heads/tophat_bear_head.png"))
    heads.append(load("res://assets/Bears/Heads/drooling.png"))
    heads.append(load("res://assets/Bears/Heads/gentlesir_face.png"))
    heads.append(load("res://assets/Bears/Heads/hat_face.png"))
    heads.append(load("res://assets/Bears/Heads/horror_face.png"))
    heads.append(load("res://assets/Bears/Heads/moustache_face.png"))
    heads.append(load("res://assets/Bears/Heads/O_face.png"))
    heads.append(load("res://assets/Bears/Heads/pleasant_face.png"))
    heads.append(load("res://assets/Bears/Heads/shocked_sad_face.png"))
    heads.append(load("res://assets/Bears/Heads/sunglasses_face.png"))
    heads.append(load("res://assets/Bears/Heads/uni_face.png"))

    hats.append(load("res://assets/Bears/Hats/beanie.png"))
    hats.append(load("res://assets/Bears/Hats/arthur.png"))
    hats.append(load("res://assets/Bears/Hats/bearable.png"))
    hats.append(load("res://assets/Bears/Hats/blindfold.png"))
    hats.append(load("res://assets/Bears/Hats/cap.png"))
    hats.append(load("res://assets/Bears/Hats/dragonborn.png"))
    hats.append(load("res://assets/Bears/Hats/duck.png"))
    hats.append(load("res://assets/Bears/Hats/flower.png"))
    hats.append(load("res://assets/Bears/Hats/fsh.png"))
    hats.append(load("res://assets/Bears/Hats/gummybear.png"))
    hats.append(load("res://assets/Bears/Hats/sombrero.png"))
    hats.append(load("res://assets/Bears/Hats/spinny.png"))
    hats.append(load("res://assets/Bears/Hats/sprout.png"))
    hats.append(load("res://assets/Bears/Hats/toppy.png"))
    hats.append(load("res://assets/Bears/Hats/link.png"))
    hats.append(load("res://assets/Bears/Hats/beario.png"))
    hats.append(load("res://assets/Bears/Hats/bearuigi.png"))
    hats.append(load("res://assets/Bears/Hats/hoddog.png"))
    hats.append(load("res://assets/Bears/Hats/mastchief.png"))
    hats.append(load("res://assets/Bears/Hats/nest.png"))
    hats.append(load("res://assets/Bears/Hats/trojan.png"))


    walking.append(load("res://assets/Bears/Bodies/walking_body_1.png"))
    walking.append(load("res://assets/Bears/Bodies/walking_body_2.png"))


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    pass
