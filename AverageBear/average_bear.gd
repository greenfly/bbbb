extends RigidBody2D
class_name AverageBear;

var screen_size # Size of the game window.
var all_chats = []

func _ready():
    screen_size = get_viewport_rect().size
    randomize()
    all_chats = $Chats.get_children()
    randomize_chat_wait()
    $ChatTimer.start()

func randomize_chat_wait():
    $ChatTimer.wait_time = randf_range(2.0, 5.0)

func _on_body_entered(body:Node):
    Events.emit_signal("bear_hit_something", self, body)

func _physics_process(delta):
    if $ChatTimer.is_stopped() and (all_chats.size() > 0):
        randomize_chat_wait()
        $ChatTimer.start()
        var chosen_random_chat = all_chats[randi()% all_chats.size()]
        chosen_random_chat.play()
    if(position.x > screen_size.x or position.y > screen_size.y):
        if(!self.is_queued_for_deletion()):
            _bear_has_left()

func _bear_has_left():
    Events.emit_signal("bear_left", position)
    queue_free()
