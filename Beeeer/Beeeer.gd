extends RigidBody2D

var delete_me = false

func _ready():
    delete_me = false

func _physics_process(_delta):
    if delete_me:
        if not $BottleBreak.playing:
            queue_free()

func _on_body_entered(body:Node):
    Events.emit_signal("beer_hit_something", self, body)
    $BottleBreak.play()
    hide()
    delete_me = true
